<?php

namespace RealestateBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('advert', TextType::class, [
            'label' => false,
            'required'  => false,
            'attr' => ['placeholder' => 'Search of Properties']
        ])->add('property', ChoiceType::class, [
            'choices'  => [
                'Appartment' => 0,
                'Building' => 1,
                'Office' => 2,
            ],
            'placeholder' => 'Property',
            'label' => false,
            'required'  => false,
        ])->add('price', ChoiceType::class, [
            'choices'  => [
                '$150,000 - $200,000' => '150000-200000',
                '$200,000 - $250,000' => '200000-250000',
                '$250,000 - $300,000' => '250000-300000',
                '$300,000 - above' => '300000'
            ],
            'placeholder' => 'Price',
            'label' => false,
            'required'  => false,
        ]);
    }
}
<?php

namespace RealestateBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvertsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('price', TextType::class, [
                      'label' => 'Price',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('address', TextType::class, [
                      'label' => 'Address',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                /*->add('location')*/
                ->add('bedroom', TextType::class, [
                      'label' => 'Bedroom',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('livingroom', TextType::class, [
                      'label' => 'Livingroom',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('parking', TextType::class, [
                      'label' => 'Parking',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('kitchen', TextType::class, [
                      'label' => 'Kitchen',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('description', TextareaType::class, [
                      'label' => 'Description',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])

                ->add('hot', ChoiceType::class, [
                      'choices' => [
                          'Yes' => 1,
                          'No' => 0
                      ],
                      'expanded' => true,
                      'label' => 'Hot',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('sold', ChoiceType::class, [
                      'choices' => [
                          'Yes' => 1,
                          'No' => 0
                      ],
                      'expanded' => true,
                      'label' => 'Sold',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('type', ChoiceType::class, [
                      'choices' => [
                          'Apartment' => 0,
                          'Building' => 1,
                          'Office Space' => 2
                      ],
                      'label' => 'Type',
                      'label_attr' => ['class' => 'col-md-4 control-label'],
                      ])
                ->add('recommend', ChoiceType::class, [
                    'choices' => [
                        'Yes' => 1,
                        'No' => 0
                    ],
                    'expanded' => true,
                    'label' => 'Recommend',
                    'label_attr' => ['class' => 'col-md-4 control-label'],
                ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'RealestateBundle\Entity\Adverts'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'realestatebundle_adverts';
    }


}

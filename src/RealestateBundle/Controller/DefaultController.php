<?php

namespace RealestateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/main")
     */
    /*public function indexAction()
    {
        return $this->render('RealestateBundle:Main:index.html.twig');
    }*/
}
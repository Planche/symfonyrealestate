<?php

namespace RealestateBundle\Controller;

use RealestateBundle\Entity\Adverts;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Advert controller.
 *
 * @Route("posts")
 */
class AdvertsController extends Controller
{
    /**
     * Lists all advert entities.
     *
     * @Route("/", name="posts_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $adverts = $em->getRepository('RealestateBundle:Adverts')->findAll();

        return $this->render('@Realestate/adverts/index.html.twig', array(
            'adverts' => $adverts,
        ));
    }

    /**
     * Creates a new advert entity.
     *
     * @Route("/new", name="posts_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $advert = new Adverts();
        $form = $this->createForm('RealestateBundle\Form\AdvertsType', $advert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($advert);
            $em->flush();

            return $this->redirectToRoute('posts_show', array('id' => $advert->getId()));
        }

        return $this->render('@Realestate/adverts/new.html.twig', array(
            'advert' => $advert,
            'form' => $form->createView(),
        ));
    }

    /**
     * Images page output.
     *
     * @Route("/images", name="posts_images")
     * @Method({"GET", "POST"})
     */
    public function imagesAction()
    {
        $advert = new Adverts();
        $general = $this->createForm('RealestateBundle\Form\GeneralType', $advert);
        $images = $this->createForm('RealestateBundle\Form\ImagesType', $advert);

        $csrfToken = $this->has('security.csrf.token_manager')
            ? $this->get('security.csrf.token_manager')->getToken('realestatebundle_adverts')->getValue()
            : null;

        return $this->render('@Realestate/adverts/images.html.twig',[
            'general' => $general->createView(),
            'images' => $images->createView(),
            'csrf_token' => $csrfToken,
        ]);
    }

    /**
     * Uploads general Image.
     *
     * @Route("/uploadgeneral", name="posts_upload_general")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadgeneralAction(Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $adverts = new Adverts();
            $form = $this->createForm('RealestateBundle\Form\GeneralType', $adverts);
            $form->handleRequest($request);
            $csrf = $request->get('_token');
            //$valid = $form->getErrors();


            if ($form->isSubmitted() && $form->isValid() && $this->isCsrfTokenValid('realestatebundle_adverts',$csrf))
            {
                $general = $adverts->getGeneral();
            }




            return new JsonResponse([
                'success' => true,
                'message' => 'is Ajax'
            ],200);
        }
        else
        {
            return new JsonResponse([
                'success' => false,
                'errors' => 'Not Ajax Call'
            ],422);
        }

    }

    /**
     * Uploads additional Images.
     *
     * @Route("/uploadimages", name="posts_upload_images")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadimagesAction(Request $request)
    {
        if($request->isXmlHttpRequest())
        {
            $adverts = new Adverts();
            $form = $this->createForm('RealestateBundle\Form\ImagesType', $adverts);
            $form->handleRequest($request);

            $csrf = $request->get('_token');
            //$valid = $form->getErrors();

            if ($form->isSubmitted() && $form->isValid() && $this->isCsrfTokenValid('realestatebundle_adverts',$csrf))
            {
                $images = $adverts->getGeneral();
            }


            return new JsonResponse([
                'success' => true,
                'message' => 'is Ajax'
            ],200);
        }
        else
        {
            return new JsonResponse([
                'success' => false,
                'errors' => 'Not Ajax Call'
            ],422);
        }
    }

    /**
     * Finds and displays a advert entity.
     *
     * @Route("/{id}", name="posts_show")
     * @Method("GET")
     */
    public function showAction(Adverts $advert)
    {
        $deleteForm = $this->createDeleteForm($advert);

        return $this->render('@Realestate/adverts/show.html.twig', array(
            'advert' => $advert,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing advert entity.
     *
     * @Route("/{id}/edit", name="posts_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Adverts $advert)
    {
        $deleteForm = $this->createDeleteForm($advert);
        $editForm = $this->createForm('RealestateBundle\Form\AdvertsType', $advert);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            // return $this->redirectToRoute('posts_edit', array('id' => $advert->getId()));
             return $this->redirectToRoute('posts_index');
        }

        return $this->render('@Realestate/adverts/edit.html.twig', array(
            'advert' => $advert,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a advert entity.
     *
     * @Route("/{id}", name="posts_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Adverts $advert)
    {
        $form = $this->createDeleteForm($advert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($advert);
            $em->flush();
        }

        return $this->redirectToRoute('posts_index');
    }

    /**
     * Creates a form to delete a advert entity.
     *
     * @param Adverts $advert The advert entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Adverts $advert)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('posts_delete', array('id' => $advert->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

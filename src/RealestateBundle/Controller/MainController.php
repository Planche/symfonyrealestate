<?php

namespace RealestateBundle\Controller;

use RealestateBundle\HelperTraits\LoginVariables;
use RealestateBundle\RealestateBundle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller
{
    use LoginVariables;
    /**
     * @Route("/main")
     * @Method ("GET")
     * @param $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // Search Form
        $form = $this->createForm('RealestateBundle\Form\SearchType',[]);

        $form->handleRequest($request);

        $advert = $this->getDoctrine()->getRepository('RealestateBundle:Adverts');

        // Main slider
        $adverts = $advert->findby([],['id' => 'desc'],5);
        $adverts_count = count($adverts);

        // Featured adverts(carousel)
        $featured = $advert->findBy([],[],15);

        // Recommended slider content
        $recommend = $advert->findBy(['recommend' => 1],['id' => 'desc'],5);
        $recommend_count = count($recommend);

        // Variables for Login Form
        $lastUsername = $this->getLastUsername($request);
        $error = $this->getError($request);

        return $this->render('RealestateBundle:Main:index.html.twig',[
            'form' => $form->createView(),
            'adverts' => $adverts,
            'adverts_count' => $adverts_count,
            'featured' => $featured,
            'recommend' => $recommend,
            'recommend_count' => $recommend_count,
            'last_username' => $lastUsername,
            'error' => $error,
            ]);

    }
}
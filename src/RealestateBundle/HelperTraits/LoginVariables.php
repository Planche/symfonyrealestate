<?php

namespace RealestateBundle\HelperTraits;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;

trait LoginVariables
{
    /**
     * Return last username for login form
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getLastUsername(Request $request)
    {
        $session = $request->getSession();
        $lastUsernameKey = Security::LAST_USERNAME;

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        return $lastUsername;
    }

    /**
     * Return errors for login form
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getError(Request $request)
    {
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        return $error;
    }

}
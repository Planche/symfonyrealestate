<?php

namespace RealestateBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\FileBag;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Adverts
 *
 * @ORM\Entity(repositoryClass="RealestateBundle\Repository\AdvertRepository")
 * @ORM\Table(name="adverts")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Adverts
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="fk_agent", type="integer", nullable=true)
     */
    private $fkAgent;

    /**
     * @var integer
     *
     * @ORM\Column(name="bedroom", type="smallint", nullable=true)
     */
    private $bedroom;

    /**
     * @var integer
     *
     * @ORM\Column(name="livingroom", type="smallint", nullable=true)
     */
    private $livingroom;

    /**
     * @var integer
     *
     * @ORM\Column(name="parking", type="smallint", nullable=true)
     */
    private $parking;

    /**
     * @var integer
     *
     * @ORM\Column(name="kitchen", type="smallint", nullable=true)
     */
    private $kitchen;

    /**
     * @var string
     *
     * @ORM\Column(name="general_image", type="string", length=200, nullable=true)
     */
    private $generalImage;

    /**
     * @var UploadedFile $general
     *
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    private $general;

    /**
     * @var array $images
     * @ORM\Column(type="array")
     */
    private $images;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=50, nullable=true)
     */
    private $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="hot", type="smallint", nullable=true)
     */
    private $hot;

    /**
     * @var integer
     *
     * @ORM\Column(name="sold", type="smallint", nullable=true)
     */
    private $sold;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="recommend", type="smallint", nullable=true)
     */
    private $recommend;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created = '0000-00-00 00:00:00';



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Adverts
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Adverts
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set fkAgent
     *
     * @param integer $fkAgent
     *
     * @return Adverts
     */
    public function setFkAgent($fkAgent)
    {
        $this->fkAgent = $fkAgent;

        return $this;
    }

    /**
     * Get fkAgent
     *
     * @return integer
     */
    public function getFkAgent()
    {
        return $this->fkAgent;
    }

    /**
     * Set bedroom
     *
     * @param integer $bedroom
     *
     * @return Adverts
     */
    public function setBedroom($bedroom)
    {
        $this->bedroom = $bedroom;

        return $this;
    }

    /**
     * Get bedroom
     *
     * @return integer
     */
    public function getBedroom()
    {
        return $this->bedroom;
    }

    /**
     * Set livingroom
     *
     * @param integer $livingroom
     *
     * @return Adverts
     */
    public function setLivingroom($livingroom)
    {
        $this->livingroom = $livingroom;

        return $this;
    }

    /**
     * Get livingroom
     *
     * @return integer
     */
    public function getLivingroom()
    {
        return $this->livingroom;
    }

    /**
     * Set parking
     *
     * @param integer $parking
     *
     * @return Adverts
     */
    public function setParking($parking)
    {
        $this->parking = $parking;

        return $this;
    }

    /**
     * Get parking
     *
     * @return integer
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set kitchen
     *
     * @param integer $kitchen
     *
     * @return Adverts
     */
    public function setKitchen($kitchen)
    {
        $this->kitchen = $kitchen;

        return $this;
    }

    /**
     * Get kitchen
     *
     * @return integer
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * Set generalImage
     *
     * @param string $generalImage
     *
     * @return Adverts
     */
    public function setGeneralImage($generalImage)
    {
        $this->generalImage = $generalImage;

        return $this;
    }

    /**
     * Get generalImage
     *
     * @return string
     */
    public function getGeneralImage()
    {
        return $this->generalImage;
    }

    /**
     * Set general
     *
     * @param File $general
     *
     * @return Adverts
     */
    public function setGeneral(File $general = null)
    {
        $this->general = $general;

        return $this;
    }

    /**
     * Get general
     *
     * @return File
     */
    public function getGeneral()
    {
        return $this->general;
    }

    /**
     * Set images
     *
     * @param Collection $images
     *
     * @return Adverts
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Adverts
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Adverts
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set hot
     *
     * @param integer $hot
     *
     * @return Adverts
     */
    public function setHot($hot)
    {
        $this->hot = $hot;

        return $this;
    }

    /**
     * Get hot
     *
     * @return integer
     */
    public function getHot()
    {
        return $this->hot;
    }

    /**
     * Set sold
     *
     * @param integer $sold
     *
     * @return Adverts
     */
    public function setSold($sold)
    {
        $this->sold = $sold;

        return $this;
    }

    /**
     * Get sold
     *
     * @return integer
     */
    public function getSold()
    {
        return $this->sold;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Adverts
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set recommend
     *
     * @param integer $recommend
     *
     * @return Adverts
     */
    public function setRecommend($recommend)
    {
        $this->recommend = $recommend;

        return $this;
    }

    /**
     * Get recommend
     *
     * @return integer
     */
    public function getRecommend()
    {
        return $this->recommend;
    }

    /**
     * Set updated
     *
     * @ORM\PreUpdate()
     *
     * @return Adverts
     */
    public function setUpdated()
    {
        $this->updated = new \DateTime('now');

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @ORM\PrePersist
     *
     * @return Adverts
     */
    public function setCreated()
    {
        $this->created = new \DateTime('now');

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}
